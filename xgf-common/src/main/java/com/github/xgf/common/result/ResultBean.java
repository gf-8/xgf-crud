package com.github.xgf.common.result;

import com.github.xgf.common.status.ResultStatus;
import lombok.Data;

import java.io.Serializable;

@Data
//@JsonSerialize(include =  JsonSerialize.Inclusion.NON_NULL)
public class ResultBean<T> implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -3521197421619157428L;
    private String msg = "success";
    private int code = ResultStatus.SUCCESS
            .getCode();
    private T data;

    public ResultBean() {
        super();
    }
    public ResultBean(ResultStatus resultStatus) {
        super();
        this.code = resultStatus.getCode();
        this.msg = resultStatus.getMessage();
    }

    public ResultBean(T data) {
        super();
        this.data = data;
    }

    public ResultBean(Throwable e) {
        super();
        this.msg = e.toString();
        this.code = ResultStatus.FAIL.getCode();
    }


}
