package com.github.xgf.common.util;

import org.springframework.data.domain.Sort;

/**
 * JPA SORT 排序封装类
 */
public class SortTools {
    public static Sort basicSort() {
        return basicSort("desc", "createTime");
    }

    public static Sort basicSort(String orderType, String orderField) {
        Sort sort = new Sort(Sort.Direction.fromString(orderType), orderField);
        return sort;
    }
}
