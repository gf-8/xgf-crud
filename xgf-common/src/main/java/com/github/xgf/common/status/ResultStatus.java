package com.github.xgf.common.status;

import lombok.Getter;

/**
 * 自定义请求状态码
 * @author xgf
 * @date 2018/1/18
 */
@Getter
public enum ResultStatus {
    SUCCESS(0, "成功"),
    FAIL(1, "请求失败"),
    USERNAME_OR_PASSWORD_ERROR(-1001, "用户名或密码错误"),
    USER_NOT_FOUND(-1002, "用户不存在"),
    USER_NOT_LOGIN(-1003, "用户未登录"),
    PARAMS_ERROR(-1004,"参数传递错误"),
    TOKEN_OVERTIME(-1005,"token过期");

    /**
     * 返回码
     */
    private int code;

    /**
     * 返回结果描述
     */
    private String message;

    ResultStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

}
