package com.github.xgf.common.status;

import lombok.Getter;

@Getter
public enum StatusEnum {
    NO_DELETE(0, "未删除"),
    DELETE(1, "删除");
    private int code;
    private String status;

    StatusEnum(int code, String status) {
        this.code = code;
        this.status = status;
    }


    public void setStatus(String status) {
        this.status = status;
    }
    public static String getByValue(int value) {
        for (StatusEnum tradeStatus : values()) {
            if (tradeStatus.getCode() == value) {
                return tradeStatus.getCode()+"";
            }
        }
        return null;
    }
}
