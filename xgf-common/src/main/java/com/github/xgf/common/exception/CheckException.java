package com.github.xgf.common.exception;

import com.github.xgf.common.status.ResultStatus;
import lombok.Data;

@Data
public class CheckException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private int code;
	private String msg;

	public CheckException() {
	}
	public CheckException(ResultStatus resultStatus) {
		this.code = resultStatus.getCode();
		this.msg = resultStatus.getMessage();
	}
	public CheckException(ResultStatus resultStatus,String msg) {
		this.code = resultStatus.getCode();
		this.msg = msg;
	}
	public CheckException(String message) {
		this.msg = msg;
	}

	public CheckException(Throwable cause) {
		super(cause);
	}

	public CheckException(String message, Throwable cause) {
		super(message, cause);
	}

	public CheckException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
