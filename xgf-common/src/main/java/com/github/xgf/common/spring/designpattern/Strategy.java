package com.github.xgf.common.spring.designpattern;

import java.lang.reflect.Method;
import java.util.List;

public interface Strategy {
    public List<Method> adaptionMethod(Object param, Method method) throws NoSuchMethodException;
}
