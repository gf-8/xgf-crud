package com.github.xgf.common.util;

import com.github.xgf.common.exception.CheckException;
import com.github.xgf.common.status.ResultStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;

import javax.annotation.Resource;

/**
 * xgf  2018/1/11
 *
 * @version 2.0
 */
@Slf4j
public class CheckUtil {

    private static MessageSource resources;
    @Resource
    public static void setResources(MessageSource resources) {
        CheckUtil.resources = resources;
    }

    public static void check(boolean condition, String msgKey, Object... args) {
        if (!condition) {
            fail(msgKey, args);
        }
    }

    public static void notEmpty(String str, String msgKey, Object... args) {
        if (str == null || str.isEmpty()) {
            fail(msgKey, args);
        }
    }

    public static void notNull(Object obj, String msgKey, Object... args) {
        if (obj == null) {
            fail(msgKey, args);
        }
    }

    private static void fail(String msgKey, Object... args) {
        throw new CheckException(ResultStatus.FAIL,msgKey);
    }
}