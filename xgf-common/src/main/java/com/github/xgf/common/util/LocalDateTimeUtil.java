package com.github.xgf.common.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * LocalDateTime 工具类 使用单例
 * @Author xgf
 * @Version 1.0  2018/1/18
 */
public class LocalDateTimeUtil {
    private LocalDateTimeUtil LocalDateTimeUtil = null;

    private LocalDateTimeUtil() {

    }

    public static LocalDateTimeUtil getInstance() {
        return LocalDateTimeInnerClass.LocalDateTimeUtil;
    }

    private static class LocalDateTimeInnerClass {
        private static LocalDateTimeUtil LocalDateTimeUtil = new LocalDateTimeUtil();
    }

    /**
     * 两个时间的比较，before参数 是否在after参数的后面
     *
     * @param before
     * @param after
     * @return
     */
    public boolean isAfter(LocalDateTime before, LocalDateTime after) {
        return before.isAfter(after);
    }

    public boolean isAfter(LocalDateTime after) {
        LocalDateTime localNowTime = LocalDateTime.now();
        return localNowTime.isAfter(after);
    }


    public boolean isBofore(LocalDateTime before, LocalDateTime after) {
        return before.isBefore(after);
    }

    public boolean isBofore(LocalDateTime after) {
        LocalDateTime localNowTime = LocalDateTime.now();
        return localNowTime.isBefore(after);
    }

    public LocalDateTime DateToLocalDateTime(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    /**
     * 当前时间加一段时间
     */
    public LocalDateTime plus(int plusTime, ChronoUnit chronoUnit) {
        LocalDateTime localNowTime = LocalDateTime.now();
        LocalDateTime localDateTime = localNowTime.plus(plusTime, chronoUnit);
        return localDateTime;
    }

    public Date LocaldateTimeToDate(LocalDateTime localDateTime) {
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime zdt = localDateTime.atZone(zoneId);

        Date date = Date.from(zdt.toInstant());
        return date;
    }

    /**
     * 当前时间转变为Date
     *
     * @return
     */
    public Date LocaldateTimeToDate() {
        LocalDateTime localDateTime = LocalDateTime.now();
        ZonedDateTime zdt = localDateTime.atZone(ZoneId.systemDefault());

        Date date = Date.from(zdt.toInstant());
        return date;
    }
}
