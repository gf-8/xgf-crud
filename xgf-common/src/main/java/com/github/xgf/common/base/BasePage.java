package com.github.xgf.common.base;

import lombok.Data;

@Data
public class BasePage {
    public int pageNum = 1;
    public int pageSize = 15;

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum - 1;
    }
}
