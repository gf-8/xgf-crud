package com.github.xgf.common.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.xgf.common.status.StatusEnum;
import lombok.Data;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.*;
import java.sql.Timestamp;

/**
 *
 * 数据库Entity虚拟基础类，其他所有Entities都继承它。
 * @version 1.0.0.0
 *
 */
@MappedSuperclass
@Data
abstract public class BaseJsonignoreEntity<T> {


    /**
     * 数据第一次创建时间，自动生成，无需传入
     */
    @Column(name = "create_time", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
    @Generated(GenerationTime.ALWAYS)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Timestamp createTime;

    /**
     * 每次更新此条数据时的记录时间（上一次更新时间），无需传入
     */
    @Column(name = "update_time", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP", insertable = false, updatable = false)
    @Generated(GenerationTime.ALWAYS)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Timestamp updateTime;

    /**
     *  创建人Id
     */
    @JsonIgnore
    @NotFound(action= NotFoundAction.IGNORE)
    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "create_user", referencedColumnName = "id", nullable = true)
    @LazyToOne(value = LazyToOneOption.FALSE)
    public T createUser;

    /**
     * 修改人Id
     */
    @JsonIgnore
    @NotFound(action= NotFoundAction.IGNORE)
    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "update_user", referencedColumnName = "id", nullable = true)
    @LazyToOne(value = LazyToOneOption.FALSE)
    public T updateUser;

    /**
     * 删除人
     */
    @JsonIgnore
    @NotFound(action= NotFoundAction.IGNORE)
    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "delete_user", referencedColumnName = "id", nullable = true)
    @LazyToOne(value = LazyToOneOption.FALSE)
    public T deleteUser;

    /**
     *  删除日期
     */
    @Column(name = "delete_date",nullable=true)
    @Generated(GenerationTime.ALWAYS)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Timestamp deleteDate;

    /**
     * 删除标识
     */
    @Column(name = "delete_flag", columnDefinition="INT default 0",nullable=false)
    public int deleteFlag = StatusEnum.NO_DELETE.getCode();



}
