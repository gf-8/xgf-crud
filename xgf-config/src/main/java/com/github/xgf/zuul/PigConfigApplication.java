package com.github.xgf.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author xgf
 *  2018/1/31
 */
@EnableDiscoveryClient
@EnableConfigServer //开启配置服务器的功能
@SpringBootApplication
public class PigConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(PigConfigApplication.class, args);
    }
}
