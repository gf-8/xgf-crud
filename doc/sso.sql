/*
Navicat MySQL Data Transfer

Source Server         : 1
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : sso

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-02-02 15:19:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mst_authority
-- ----------------------------
DROP TABLE IF EXISTS `mst_authority`;
CREATE TABLE `mst_authority` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL DEFAULT '0',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `delete_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKojb99yf1cil5ev061yiuuiqq0` (`create_user`),
  KEY `FK2r1usdt61gs8i8ob1yi49kyo7` (`delete_user`),
  KEY `FKnndslqtb8stk0ruoyevabltuv` (`update_user`),
  CONSTRAINT `mst_authority_ibfk_1` FOREIGN KEY (`delete_user`) REFERENCES `mst_user` (`id`),
  CONSTRAINT `mst_authority_ibfk_2` FOREIGN KEY (`update_user`) REFERENCES `mst_user` (`id`),
  CONSTRAINT `mst_authority_ibfk_3` FOREIGN KEY (`create_user`) REFERENCES `mst_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_authority
-- ----------------------------
INSERT INTO `mst_authority` VALUES ('1', '2018-02-02 14:32:32', null, '0', '2018-02-02 14:32:32', '查询', 'query', null, null, null);

-- ----------------------------
-- Table structure for mst_role
-- ----------------------------
DROP TABLE IF EXISTS `mst_role`;
CREATE TABLE `mst_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL DEFAULT '0',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `delete_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKb80d92xr0mnashw3mt6nmy6h5` (`create_user`),
  KEY `FKen74a9k5g4v3ffvh4uwhgatmn` (`delete_user`),
  KEY `FKd490vo8nig1wbmlhoesaqdo4y` (`update_user`),
  CONSTRAINT `mst_role_ibfk_1` FOREIGN KEY (`create_user`) REFERENCES `mst_user` (`id`),
  CONSTRAINT `mst_role_ibfk_2` FOREIGN KEY (`update_user`) REFERENCES `mst_user` (`id`),
  CONSTRAINT `mst_role_ibfk_3` FOREIGN KEY (`delete_user`) REFERENCES `mst_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_role
-- ----------------------------
INSERT INTO `mst_role` VALUES ('1', '2018-02-02 14:32:32', null, '0', '2018-02-02 14:32:32', '管理员', 'ROLE_ADMIN', null, null, null);
INSERT INTO `mst_role` VALUES ('2', '2018-02-02 14:32:32', null, '0', '2018-02-02 14:32:32', '普通用户', 'ROLE_USER', null, null, null);

-- ----------------------------
-- Table structure for mst_role_authorities
-- ----------------------------
DROP TABLE IF EXISTS `mst_role_authorities`;
CREATE TABLE `mst_role_authorities` (
  `mst_role_id` bigint(20) NOT NULL,
  `authorities_id` bigint(20) NOT NULL,
  PRIMARY KEY (`mst_role_id`,`authorities_id`),
  KEY `FK78n2xyiuthf9gk0q6rgt495w0` (`authorities_id`),
  CONSTRAINT `mst_role_authorities_ibfk_1` FOREIGN KEY (`authorities_id`) REFERENCES `mst_authority` (`id`),
  CONSTRAINT `mst_role_authorities_ibfk_2` FOREIGN KEY (`mst_role_id`) REFERENCES `mst_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_role_authorities
-- ----------------------------
INSERT INTO `mst_role_authorities` VALUES ('1', '1');

-- ----------------------------
-- Table structure for mst_user
-- ----------------------------
DROP TABLE IF EXISTS `mst_user`;
CREATE TABLE `mst_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL DEFAULT '0',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email` varchar(255) DEFAULT NULL,
  `icard` varchar(255) DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `login_count` int(11) DEFAULT '0',
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `roleid` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `token_time` datetime DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `user_logo` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `delete_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8kb63cgyq03nrpwaxjwh530tk` (`create_user`),
  KEY `FK9najpppy8jv4nlxxo3g0k8o8f` (`delete_user`),
  KEY `FKlx0t7fgwlwdxh46ah0hh89nfy` (`update_user`),
  CONSTRAINT `mst_user_ibfk_1` FOREIGN KEY (`create_user`) REFERENCES `mst_user` (`id`),
  CONSTRAINT `mst_user_ibfk_2` FOREIGN KEY (`delete_user`) REFERENCES `mst_user` (`id`),
  CONSTRAINT `mst_user_ibfk_3` FOREIGN KEY (`update_user`) REFERENCES `mst_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_user
-- ----------------------------
INSERT INTO `mst_user` VALUES ('1', '2018-02-02 14:32:33', null, '0', '2018-02-02 14:32:33', null, null, null, '0', '$2a$10$zIcDB5cGv14kTTjrEL/SfOUrHQw/fT/7y.GNNUreWksfcZQhe0U/u', null, null, null, null, null, null, null, 'fpf', null, null, null);
INSERT INTO `mst_user` VALUES ('2', '2018-02-02 14:32:33', null, '0', '2018-02-02 14:32:33', null, null, null, '0', '$2a$10$jwfRSFPB4CWgUqnBt0sXNOUlQxGBm7evMF17PwKQZ9o0wAkuqsw5m', null, null, null, null, null, null, null, 'wl', null, null, null);

-- ----------------------------
-- Table structure for mst_user_roles
-- ----------------------------
DROP TABLE IF EXISTS `mst_user_roles`;
CREATE TABLE `mst_user_roles` (
  `mst_user_id` bigint(20) NOT NULL,
  `roles_id` bigint(20) NOT NULL,
  PRIMARY KEY (`mst_user_id`,`roles_id`),
  KEY `FKd4wm251ftfkd6pd31qo98v062` (`roles_id`),
  CONSTRAINT `mst_user_roles_ibfk_1` FOREIGN KEY (`roles_id`) REFERENCES `mst_role` (`id`),
  CONSTRAINT `mst_user_roles_ibfk_2` FOREIGN KEY (`mst_user_id`) REFERENCES `mst_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_user_roles
-- ----------------------------
INSERT INTO `mst_user_roles` VALUES ('1', '1');
INSERT INTO `mst_user_roles` VALUES ('2', '2');
