/*
Navicat MySQL Data Transfer

Source Server         : 1
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : crud

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-02-02 15:19:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mst_dept
-- ----------------------------
DROP TABLE IF EXISTS `mst_dept`;
CREATE TABLE `mst_dept` (
  `dept_no` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL DEFAULT '0',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dept_grade` int(11) DEFAULT NULL,
  `dept_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `organizeid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `delete_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dept_no`),
  KEY `FK3vfxosmt6hgmambd9nr6nmg4i` (`create_user`),
  KEY `FKsdowoflolkamh7jkpabwccr20` (`delete_user`),
  KEY `FKmeogoygtpni2yjtmruaft6d7p` (`update_user`),
  CONSTRAINT `mst_dept_ibfk_1` FOREIGN KEY (`create_user`) REFERENCES `mst_user` (`id`),
  CONSTRAINT `mst_dept_ibfk_2` FOREIGN KEY (`update_user`) REFERENCES `mst_user` (`id`),
  CONSTRAINT `mst_dept_ibfk_3` FOREIGN KEY (`delete_user`) REFERENCES `mst_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of mst_dept
-- ----------------------------
INSERT INTO `mst_dept` VALUES ('1qweqw1', '2018-01-29 10:22:25', null, '1', '2018-01-29 10:24:15', '1', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for mst_user
-- ----------------------------
DROP TABLE IF EXISTS `mst_user`;
CREATE TABLE `mst_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL DEFAULT '0',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `icard` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `login_count` int(11) DEFAULT '0',
  `login_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `login_pwd` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `roleid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `token_time` datetime DEFAULT NULL,
  `userid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_logo` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `delete_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `dept_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8kb63cgyq03nrpwaxjwh530tk` (`create_user`),
  KEY `FK9najpppy8jv4nlxxo3g0k8o8f` (`delete_user`),
  KEY `FKlx0t7fgwlwdxh46ah0hh89nfy` (`update_user`),
  KEY `FKhii2lwbgghjovpkxf367qd9an` (`dept_id`),
  CONSTRAINT `mst_user_ibfk_1` FOREIGN KEY (`create_user`) REFERENCES `mst_user` (`id`),
  CONSTRAINT `mst_user_ibfk_2` FOREIGN KEY (`delete_user`) REFERENCES `mst_user` (`id`),
  CONSTRAINT `mst_user_ibfk_3` FOREIGN KEY (`dept_id`) REFERENCES `mst_dept` (`dept_no`),
  CONSTRAINT `mst_user_ibfk_4` FOREIGN KEY (`update_user`) REFERENCES `mst_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of mst_user
-- ----------------------------
INSERT INTO `mst_user` VALUES ('1', '2018-01-29 10:22:46', null, '0', '2018-01-30 16:01:23', null, null, null, '0', '1', '1', '1', null, '19da586be5e3436ab7561766cf75f8a1', '2018-02-02 16:01:23', '1', null, '1', null, null, null, null, '1qweqw1');
INSERT INTO `mst_user` VALUES ('2', '2018-01-29 10:24:55', null, '1', '2018-01-29 10:25:06', null, null, null, '0', '2', '2', '2', null, null, null, null, null, '2', null, null, null, null, '1qweqw1');

-- ----------------------------
-- Table structure for school
-- ----------------------------
DROP TABLE IF EXISTS `school`;
CREATE TABLE `school` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL DEFAULT '0',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_user` bigint(20) DEFAULT NULL,
  `delete_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKq1pjjhay2yd455glpa36xj1lk` (`create_user`),
  KEY `FKs2flm9ab9m7ds5w5ynxetv6g4` (`delete_user`),
  KEY `FKcejnk4rel5pi6p34wq8ubk1us` (`update_user`),
  CONSTRAINT `school_ibfk_1` FOREIGN KEY (`update_user`) REFERENCES `mst_user` (`id`),
  CONSTRAINT `school_ibfk_2` FOREIGN KEY (`create_user`) REFERENCES `mst_user` (`id`),
  CONSTRAINT `school_ibfk_3` FOREIGN KEY (`delete_user`) REFERENCES `mst_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of school
-- ----------------------------
INSERT INTO `school` VALUES ('1', '2018-01-31 08:53:40', null, '0', '2018-01-31 08:53:40', null, null, null);
INSERT INTO `school` VALUES ('2', '2018-01-31 08:57:56', null, '0', '2018-01-31 08:57:56', null, null, null);

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL DEFAULT '0',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `school_id` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `delete_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrqso5jy5es3vavcb9asya1r3h` (`create_user`),
  KEY `FKm98cyv5d2361dccc5wmnyitum` (`delete_user`),
  KEY `FK3f5cnr049xyf8mb5c5y2q7xc6` (`update_user`),
  KEY `FK1vm0oqhk9viil6eocn49rj1l9` (`school_id`),
  CONSTRAINT `student_ibfk_1` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`),
  CONSTRAINT `student_ibfk_2` FOREIGN KEY (`update_user`) REFERENCES `mst_user` (`id`),
  CONSTRAINT `student_ibfk_3` FOREIGN KEY (`delete_user`) REFERENCES `mst_user` (`id`),
  CONSTRAINT `student_ibfk_4` FOREIGN KEY (`create_user`) REFERENCES `mst_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of student
-- ----------------------------
