package com.xgf.demo.service.impl;

import com.xgf.demo.entity.School;
import com.xgf.demo.entity.Student;
import com.xgf.demo.repository.SchoolRespository;
import com.xgf.demo.service.remote.RemoteService;
import feign.Feign;
import feign.Request;
import feign.Retryer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SchoolServiceImplTest {
    @Autowired
    private SchoolRespository schoolRespository;

    @Test
    public void save() throws Exception {
        School school = new School();
        Set<Student> set = new HashSet<>();
        set.add(new Student("1"));
        set.add(new Student("2"));
        set.add(new Student("3"));
        schoolRespository.save(school);
    }

    @Test
    public void findAll() throws Exception {
        List<School> s = schoolRespository.findAll();
        System.out.print(s);

    }


    @Test
    public void delete() throws Exception {
        schoolRespository.delete(Long.parseLong("1"));

    }

    @Autowired
    private RemoteService remoteService;

    @Test
    public void RemoteService() {
        String result = remoteService.getOwner("scott");
        System.out.print(result);
    }

    @Test
    public void testConverter() {
        DefaultConversionService serivce = new DefaultConversionService();
        boolean actual = serivce.canConvert(String.class, Boolean.class);
        Assert.assertEquals(true, actual);
        Object acc = serivce.convert("true", Boolean.class);
        Assert.assertEquals(true, ((Boolean)acc).booleanValue());
    }

}