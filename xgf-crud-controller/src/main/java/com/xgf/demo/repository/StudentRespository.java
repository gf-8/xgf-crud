package com.xgf.demo.repository;

import com.xgf.demo.entity.Student;
import com.xgf.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRespository extends JpaRepository<Student, Long> {
}
