package com.xgf.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xgf.demo.entity.User;

import java.util.Date;

@Repository
public interface UserRespository extends JpaRepository<User, Long>{
    public User findByLoginName(String username);
    public User findByToken(String token);
    @Query(value = "update mst_user set delete_flag = 1,delete_date = now(), delete_user = ?2  where id = ?1",nativeQuery = true)
    @Modifying
    public void deleteById(Long id, String userId);

    @Query(value = "select * from user where id = ?1",nativeQuery = true)
    public User findOneById(Long id);
}
