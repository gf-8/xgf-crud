package com.xgf.demo.repository;

import com.xgf.demo.entity.School;
import com.xgf.demo.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface SchoolRespository extends JpaRepository<School, Long>,JpaSpecificationExecutor<School> {
    @Query(value = "update school set delete_flag = 1,delete_date = now(), delete_user = ?2  where id = ?1",nativeQuery = true)
    @Modifying
    public void deleteById(Long id, String userId);
}
