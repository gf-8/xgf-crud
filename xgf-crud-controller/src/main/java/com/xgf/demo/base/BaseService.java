package com.xgf.demo.base;



import org.slf4j.LoggerFactory;

public abstract class BaseService<T> implements BaseMethods<T>{
    protected static org.slf4j.Logger logger = null;

    public BaseService() {
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    public void setInfo(String source, String control, String successed) {
        logger.info(source + " | " + control + " | " + successed);
    }

    public void setError(String source, String control, String successed) {
        logger.error(source + " | " + control + " | " + successed);
    }



}
