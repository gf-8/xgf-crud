package com.xgf.demo.base;

import com.xgf.demo.model.PageModel;
import org.springframework.data.domain.Page;

public interface BaseMethods<T> {
    public Long save(T t);

    /**
     * shan
     *
     * @return
     */
    public Boolean deleteById(Long id);

    /**
     * update
     *
     * @return
     */
    public Long update(T t);

    /**
     * findOne
     *
     * @return
     * @Param id
     */
    public T findById(Long id);

    /**
     * findAll 分页
     *
     * @return
     * @Param id
     */
    public Page<T> findAll(PageModel<T> page);
}
