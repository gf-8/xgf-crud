package com.xgf.demo.configuration;

import org.springframework.beans.factory.config.MethodInvokingFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

/**
 * 中英文转换类
 * 先注释掉，后面需要在加
 */
//@Configuration
public class MethodInvokingFactoryBeanConfiguration {
    @Bean
    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        String[] basenames = {"zh","format", "exceptions", "windows"};
        resourceBundleMessageSource.setBasenames(basenames);
        return resourceBundleMessageSource;
    }

    @Bean
    public MethodInvokingFactoryBean resources() {
        MethodInvokingFactoryBean methodInvokingFactoryBean = new MethodInvokingFactoryBean();
        methodInvokingFactoryBean.setStaticMethod("com.xgf.demo.util.CheckUtil.setResources");
        methodInvokingFactoryBean.setArguments(messageSource());
        return methodInvokingFactoryBean;
    }
}
