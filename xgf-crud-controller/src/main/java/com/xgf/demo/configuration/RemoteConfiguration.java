package com.xgf.demo.configuration;

import com.xgf.demo.DbTokenmanager;
import com.xgf.demo.authorization.TokenManager;
import com.xgf.demo.service.remote.RemoteClassService;
import com.xgf.demo.service.remote.RemoteService;
import feign.Feign;
import feign.Request;
import feign.Retryer;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.DefaultConversionService;

/**
 * 链接别的服务器 接口 的 配置类
 */
@Configuration
public class RemoteConfiguration {
   
    /**
     * 决定用哪个TokenManager 目前提供Redis 和 数据库两种，大家如果还有别的可以自己上传
     * @Author xgf
     * @return
     */
    @Bean
    public TokenManager createTokenManager(){
        return new DbTokenmanager();
    }

    @Bean
    public RemoteService createRemoteService() {
        return Feign.builder()
                .options(new Request.Options(1000, 3500))
                .retryer(new Retryer.Default(5000, 5000, 3))
                .target(RemoteService.class, "http://127.0.0.1:8087/rtm_fireExtinguisherSystem");
    }

    @Bean
    public RemoteClassService createRemoteClassService() {
        return  Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .options(new Request.Options(1000, 3500))
                .retryer(new Retryer.Default(5000, 5000, 3))
                .target(RemoteClassService.class, "http://127.0.0.1:8087/rtm_fireExtinguisherSystem");

    }

}
