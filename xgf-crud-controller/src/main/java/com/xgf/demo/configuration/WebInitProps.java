package com.xgf.demo.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 初始化文件信息（目前是application-dev.yml）
 */
@Data
@Component
@ConfigurationProperties(prefix="webInit") //接收application.yml中的myProps下面的属性
public class WebInitProps {
    private Boolean ifAspect;//是否开启切面的自动注入功能
}