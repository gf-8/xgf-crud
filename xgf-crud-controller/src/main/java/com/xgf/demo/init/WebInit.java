package com.xgf.demo.init;

import com.github.xgf.common.util.PackageUtil;
import com.xgf.demo.spring.convert.StringToDateConverter;
import com.xgf.demo.spring.convert.StringToStudentConverter;
import com.xgf.demo.spring.designpattern.DeleteStrategy;
import com.xgf.demo.spring.designpattern.SaveStrategy;
import com.xgf.demo.spring.designpattern.UpdateStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterRegistry;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.WebBindingInitializer;
import org.springframework.web.context.request.WebRequest;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

@Configuration
public class WebInit extends  DefaultConversionService {
    @Autowired
    private ConverterRegistry converterRegistry;
    @Bean
    public ConverterRegistry createConverterRegistry(){
        converterRegistry.addConverter(new StringToStudentConverter());
        converterRegistry.addConverter(new StringToDateConverter());
        /**
         * 自动填充变量的检索包
         */
        PackageUtil.packageNames = PackageUtil.getInstance().getClass("com.xgf.demo.entity",true);
        Map methodMap = new HashMap<>();
        methodMap.put("save",new SaveStrategy());
        methodMap.put("update",new UpdateStrategy());
        methodMap.put("delete",new DeleteStrategy());
        PackageUtil.methodMap =methodMap;
        return converterRegistry;
    }

}
