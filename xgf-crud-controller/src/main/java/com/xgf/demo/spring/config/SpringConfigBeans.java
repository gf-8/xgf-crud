package com.xgf.demo.spring.config;

import com.xgf.demo.spring.convert.StringToDateConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.web.bind.support.ConfigurableWebBindingInitializer;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import javax.annotation.PostConstruct;
import java.util.Date;

@Configuration
public class SpringConfigBeans {
    @Autowired
    private RequestMappingHandlerAdapter requestMappingHandlerAdapter;
    @PostConstruct
    public void init(){
        ConfigurableWebBindingInitializer initializer = (ConfigurableWebBindingInitializer)requestMappingHandlerAdapter.getWebBindingInitializer();
        if(initializer.getConversionService() != null){
           GenericConversionService genericConversionService =  (GenericConversionService) initializer.getConversionService();
            genericConversionService.addConverter(new StringToDateConverter());
        }
    }
}
