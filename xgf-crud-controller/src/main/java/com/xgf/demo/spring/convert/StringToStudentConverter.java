package com.xgf.demo.spring.convert;

import com.xgf.demo.entity.Student;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 字符串转Student的转换器
 * @version $Id: StringToDateConverter.java, v 0.1
 */
@Component
public class StringToStudentConverter implements   Converter<String, Student> {

    private static final String dateFormat      = "yyyy-MM-dd HH:mm:ss";
    private static final String shortDateFormat = "yyyy-MM-dd";
    @Override
    public Student convert(String source) {
        //将String类型转化为Student类型
        //source形式: 123-tom-男-福建省-泉州市
        Student student = null;
        if(source!=null){
            String[] values = source.split("-");
            if(values!=null && values.length==5){
                student = new Student();
                student.setId(Long.parseLong(values[0]));
            }
        }
        return student;
    }
}