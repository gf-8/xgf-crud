package com.xgf.demo.spring.designpattern;

import com.github.xgf.common.spring.designpattern.Strategy;
import com.xgf.demo.entity.User;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class DeleteStrategy implements Strategy {
    @Override
    public List<Method> adaptionMethod(Object param, Method method) throws NoSuchMethodException {
        List<Method> methodList = new ArrayList<>();
        Method m2 = param.getClass().getSuperclass().getDeclaredMethod("setDeleteUser", User.class);
        methodList.add(m2);
        return methodList;
    }
}

