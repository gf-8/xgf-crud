package com.xgf.demo.authorization;

import com.github.xgf.common.exception.UnloginException;
import com.xgf.demo.authorization.annotation.Authorization;
import com.xgf.demo.authorization.config.Constants;
import com.xgf.demo.authorization.model.TokenModel;
import com.xgf.demo.configuration.WebInitProps;
import com.xgf.demo.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * 自定义拦截器，判断此次请求是否有权限
 */
@Component
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private TokenManager manager;

    @Autowired
    private WebInitProps webInitProps;

    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        if (webInitProps.getIfAspect()) {
            this.filter(handler, request);
        }
        return true;
    }

    private Object filter(Object handler, HttpServletRequest request) {
        //如果不是映射到方法直接通过
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        handler.getClass().getAnnotation(Authorization.class);
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        //如果没有@Authorization 注解，直接通过
        if (handlerMethod.getBeanType().getAnnotation(Authorization.class) == null
                && method.getAnnotation(Authorization.class) == null) {
            return true;
        }
        //从header中得到token
        String authorization = request.getHeader(Constants.AUTHORIZATION);
        //验证token
        TokenModel model = manager.getToken(authorization);
        if (manager.checkToken(model)) {
            //如果token验证成功，将token对应的用户id存在request中，便于之后注入
            request.setAttribute(Constants.CURRENT_USER_ID, model.getUserId());
            UserUtil.getInstance().fillUserInfo(request);
            return true;
        }
        throw new UnloginException("您尚未登录！");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
        UserUtil.getInstance().clearAllUserInfo();
    }
}
