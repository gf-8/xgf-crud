package com.xgf.demo.controller;

import com.alibaba.fastjson.annotation.JSONField;
import com.github.xgf.common.result.ResultBean;
import com.xgf.demo.authorization.annotation.Authorization;
import com.xgf.demo.entity.School;
import com.xgf.demo.entity.Student;
import com.xgf.demo.entity.User;
import com.xgf.demo.model.PageModel;
import com.xgf.demo.service.SchoolService;
import com.xgf.demo.service.UserService;
import com.xgf.demo.service.impl.SchoolServiceImpl;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
@CrossOrigin
@RestController
@Authorization
@RequestMapping("/school")
@Api(value = "/school", description = "学校")
public class SchoolController {
    @Autowired
    private SchoolServiceImpl schoolService;

    @ApiOperation(value = "获取学校列表", notes = "")
    @PostMapping(value = "/findAll", produces = {"application/json;charset=utf-8"})
    @ApiImplicitParams({ @ApiImplicitParam(name = "date", value = "测试时间", required = true, dataType = "String", paramType = "path")})
    public ResultBean<?> findAll(@ApiParam(required = true, value = "参数:时间测试") Date date, @ApiParam(required = true, value = "参数:分页加查询条件对象") @RequestBody  PageModel<School> page) {
        return new ResultBean<Page<School>>(schoolService.findAll(page));
    }
    @ApiOperation(value = "获取学校详细信息", notes = "根据url的id来获取用户详细信息")
    @ApiImplicitParam(name = "id", value = "学校ID", required = true, dataType = "Long", paramType = "path")
    @GetMapping(value = "/{id}",produces = {"application/json;charset=utf-8"})
    public ResultBean<?> findById(@ApiParam(required = true, value = "需要参数:id") @PathVariable Long id) {
        return new ResultBean<School>(schoolService.findById(id));
    }

    @ApiOperation(value = "创建", notes = "根据School对象创建")
    @ApiImplicitParam(name = "school", value = "学校详细实体school", required = true, dataType = "School")
    @PostMapping(value = "/",produces = {"application/json;charset=utf-8"})
    public ResultBean<?> save(@ApiParam(required = true, value = "需要参数:新增对象") @RequestBody School school) {
        return new ResultBean<Long>(schoolService.save(school));
    }

    @ApiOperation(value = "修改信息", notes = "根据url的id来指定删除对象")
    @ApiImplicitParam(name = "school", value = "用户详细实体school", required = true, dataType = "School")
    @PutMapping(value = "/{id}",produces = {"application/json;charset=utf-8"})
    public ResultBean<?> updateById(@ApiParam(required = true, value = "需要参数:对象") @RequestBody School school) {
        return new ResultBean<Long>(schoolService.update(school));
    }

    @ApiOperation(value = "删除信息", notes = "根据url的id来指定删除对象")
    @ApiImplicitParam(name = "id", value = "学校ID", required = true, dataType = "Long", paramType = "path")
    @DeleteMapping(value = "/{id}",produces = {"application/json;charset=utf-8"})
    public ResultBean<?> deleteById(@ApiParam(required = true, value = "需要参数:id") @PathVariable Long id) {
        return new ResultBean<Boolean>(schoolService.deleteById(id));
    }

}
