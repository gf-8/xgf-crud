package com.xgf.demo.controller;

import java.util.List;

import com.github.xgf.common.result.ResultBean;
import com.xgf.demo.authorization.annotation.Authorization;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.xgf.demo.entity.User;
import com.xgf.demo.service.UserService;
@CrossOrigin
@RestController
@Authorization
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @ApiOperation(value = "获取用户列表", notes = "")
    @GetMapping("")
    public ResultBean<?> findAll() {
        return new ResultBean<List<User>>(userService.findAll());
    }

    @ApiOperation(value = "获取用户详细信息", notes = "根据url的id来获取用户详细信息")
    @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "Long", paramType = "path")
    @GetMapping(value = "/{id}",produces = {"application/json;charset=utf-8"})
    public ResultBean<?> findById(@PathVariable Long id) {
        return new ResultBean<User>(userService.findById(id));
    }

    @ApiOperation(value = "创建用户", notes = "根据User对象创建用户")
    @ApiImplicitParam(name = "user", value = "用户详细实体user", required = true, dataType = "User")
    @PostMapping(value = "",produces = {"application/json;charset=utf-8"})
    public ResultBean<?> save(@RequestBody User user) {
        return new ResultBean<Long>(userService.save(user));
    }

    @ApiOperation(value = "修改用户", notes = "根据url的id来指定删除对象")
    @ApiImplicitParam(name = "user", value = "用户详细实体user", required = true, dataType = "User")
    @PutMapping(value = "/{id}",produces = {"application/json;charset=utf-8"})
    public ResultBean<?> updateById(@RequestBody User user) {
        return new ResultBean<Long>(userService.save(user));
    }

    @ApiOperation(value = "删除用户", notes = "根据url的id来指定删除对象")
    @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "Long", paramType = "path")
    @DeleteMapping(value = "/{id}",produces = {"application/json;charset=utf-8"})
    public ResultBean<?> deleteById(@PathVariable Long id) {
        return new ResultBean<Boolean>(userService.deleteById(id));
    }

}
