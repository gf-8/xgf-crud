package com.xgf.demo.controller;

import com.github.xgf.common.exception.CheckException;
import com.github.xgf.common.result.ResultBean;
import com.github.xgf.common.status.ResultStatus;
import com.xgf.demo.authorization.TokenManager;
import com.xgf.demo.authorization.annotation.Authorization;
import com.xgf.demo.authorization.annotation.CurrentUser;
import com.xgf.demo.authorization.model.TokenModel;
import com.xgf.demo.entity.User;
import com.xgf.demo.model.ResultModel;
import com.xgf.demo.repository.UserRespository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

/**
 * 获取和删除token的请求地址，在Restful设计中其实就对应着登录和退出登录的资源映射
 */
@CrossOrigin
@RestController
@Api(value = "/tokens", description = "登录得到token,删除token")
@RequestMapping("/tokens")
public class TokenController {

    @Autowired
    private UserRespository userRepository;

    @Autowired
    private TokenManager tokenManager;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "登录")
    public ResultBean<ResultModel> login(@RequestParam String username, @RequestParam String password) {
        Assert.notNull(username, "username can not be empty");
        Assert.notNull(password, "password can not be empty");

        User user = userRepository.findByLoginName(username);
        if (user == null ||  //未注册
                !user.getLoginPwd().equals(password)) {  //密码错误
            //提示用户名或密码错误
            throw new CheckException(ResultStatus.USER_NOT_FOUND);
        }
        //生成一个token，保存用户登录状态
        TokenModel model = tokenManager.createToken(user);
        return new ResultBean(model);
    }


    @RequestMapping(method = RequestMethod.DELETE)
    @Authorization
    @ApiOperation(value = "退出登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorization", value = "authorization", required = true, dataType = "string", paramType = "header"),
    })
    public ResultBean<ResultModel> logout(@CurrentUser User user) {
        tokenManager.deleteToken(user.getId());
        return new ResultBean();
    }

}
