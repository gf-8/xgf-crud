package com.xgf.demo.model;

import com.github.xgf.common.base.BasePage;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class PageModel<T> {
    private BasePage page;
    private T queryCriteria;
}
