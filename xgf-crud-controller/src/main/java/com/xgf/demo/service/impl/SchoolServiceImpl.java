package com.xgf.demo.service.impl;

import com.github.xgf.common.util.SortTools;
import com.xgf.demo.base.BaseService;
import com.xgf.demo.entity.School;
import com.xgf.demo.model.PageModel;
import com.xgf.demo.repository.SchoolRespository;
import com.xgf.demo.service.SchoolService;
import com.xgf.demo.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import javax.transaction.Transactional;


import java.util.ArrayList;
import java.util.List;

import static com.github.xgf.common.util.CheckUtil.check;

@Service
public class SchoolServiceImpl extends BaseService<School> {
    @Autowired
    private SchoolRespository schoolRespository;

    /**
     * zeng
     *
     * @param school
     * @return
     */
    @Override
    public Long save(School school) {
        school.setId(null);
        return schoolRespository.save(school).getId();
    }

    /**
     * shan
     *
     * @return
     */
    @Override
    @Transactional
    public Boolean deleteById(Long id) {
        check(id > 0L, "参数不对", id);
        check(schoolRespository.findOne(id) != null, "id对象不存在", id);
        schoolRespository.deleteById(id, UserUtil.getInstance().getUserId());
        return true;
    }

    /**
     * update
     *
     * @param school
     * @return
     */
    @Override
    public Long update(School school) {
        check(schoolRespository.findOne(school.getId()) != null, "id对象不存在", school.getId());
        return schoolRespository.save(school).getId();
    }

    /**
     * findOne
     *
     * @return
     * @Param id
     */
    @Override
    public School findById(Long id) {
        check(id > 0L, "参数不对", id);
        check(schoolRespository.findOne(id) != null, "id对象不存在", id);
        return schoolRespository.findOne(id);
    }

    /**
     * findAll 分页
     *
     * @return
     * @Param id
     */
    @Override
    public Page<School> findAll(PageModel<School> page) {
        Pageable pageable = new PageRequest(page.getPage().getPageNum(), page.getPage().getPageSize(), SortTools.basicSort());  //分页信息

        Specification<School> spec = new Specification<School>() {        //查询条件构造


            @Override
            public Predicate toPredicate(Root<School> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                School schoolQueryCriteria = page.getQueryCriteria();
                List<Predicate> predicateList = new ArrayList<>();
                Predicate predicate = null;
                //查询条件为空
                if (page.getQueryCriteria() == null) {
                    return predicate;
                }
                if ((schoolQueryCriteria.getId() != null) || schoolQueryCriteria.getId() > 0) {
                    predicate = cb.equal(root.get("id").as(Long.class), schoolQueryCriteria.getId());
                    predicateList.add(predicate);
                }
                predicate = cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
                return predicate;
            }

        };

        return schoolRespository.findAll(spec, pageable);

    }
}
