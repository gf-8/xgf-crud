package com.xgf.demo.service;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.xgf.demo.entity.User;

@org.springframework.stereotype.Service
public interface UserService {
	public List<User> findAll();

	public User findById(Long id);

	public Long save(User user);

	public User updateById(User user);

	public Boolean deleteById(Long id);
}
