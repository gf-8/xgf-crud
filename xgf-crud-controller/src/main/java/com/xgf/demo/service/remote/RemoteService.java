package com.xgf.demo.service.remote;

import feign.Param;
import feign.RequestLine;
import org.springframework.stereotype.Service;

@Service
public interface RemoteService {

    @RequestLine("GET /users/list?name={name}")
    String getOwner(@Param(value = "name") String name);
}
