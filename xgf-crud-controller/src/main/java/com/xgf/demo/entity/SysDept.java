package com.xgf.demo.entity;

import com.github.xgf.common.base.BaseEntity;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "mst_dept")
@Data
@SQLDelete(sql = "update mst_dept set delete_flag = 1,delete_date = now(), delete_user = ?2   where id = ?1 ")
@Where(clause = "delete_flag = 0")
public class SysDept extends BaseEntity<User> implements Serializable {
    @Id
    private String deptNo;
    @Column
    private String deptName;
    @Column
    private String organizeID;
    @Column
    private int deptGrade;
    @Column
    private String remarks;



    public SysDept() {
    }


}
