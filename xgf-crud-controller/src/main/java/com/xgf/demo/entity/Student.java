package com.xgf.demo.entity;

import com.github.xgf.common.base.BaseEntity;
import lombok.Data;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "student")
@Data
public class Student extends BaseEntity<User> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private String name;
    //在表中建立外键 "school_fk"
    @Column
    private Long school_id;
    @ManyToOne(cascade = {CascadeType.REFRESH},optional = false,fetch = FetchType.LAZY)
    @JoinColumn(name = "school_id", referencedColumnName = "id", insertable = false, updatable = false)
    @LazyToOne(value = LazyToOneOption.FALSE)
    private School school;

    public Student(String name) {
        this.name = name;
    }

    public Student() {
    }
}