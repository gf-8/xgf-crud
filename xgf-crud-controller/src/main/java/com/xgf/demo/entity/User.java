package com.xgf.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.xgf.common.base.BaseJsonignoreEntity;
import lombok.Data;
import org.hibernate.annotations.*;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;

@Entity(name = "mst_user")
@Data
@Where(clause = "delete_flag = 0")
public class User extends BaseJsonignoreEntity<User> implements Serializable {

	/**
	 * 表格主键Id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;
	@Column
	public String userID;
	@Column
	private String userName;
	@Column
	private String loginName;


	@Column
	private String loginPwd;
	@NotFound(action= NotFoundAction.IGNORE)
	@ManyToOne(cascade = {CascadeType.REFRESH},optional = false,fetch = FetchType.LAZY)
	@JoinColumn(name = "dept_id", referencedColumnName = "deptNo", insertable = false, updatable = false)
	@LazyToOne(value = LazyToOneOption.FALSE)
	private SysDept deptNo;
	@Column
	private String roleid;
	@Column
	private String userType;



	@Column
	private String userLogo;
	@Column
	private String phone;
	@Column
	private String icard;
	@Column
	private String email;
	@Column
	private Date lastLoginTime;
	@Column(columnDefinition="INT default 0")
	private int loginCount;

	@JsonIgnore
	@Column
	private String token;
	@JsonIgnore
	@Column
	private Date tokenTime;
	/**
	 * 查询用户的时候的创建人信息
	 */
	@Transient
	private User outCreateUser  = null;
	@Transient
	private User outUpdateUser  = null;
	@Transient
	private User outDeleteUser  = null;
	public User(String userid, String token, Date tokenTime) {
		this.userID = userid;
		this.token = token;
		this.tokenTime = tokenTime;
	}

	public User() {
	}
}
