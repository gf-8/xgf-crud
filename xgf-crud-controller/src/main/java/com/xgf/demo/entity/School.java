package com.xgf.demo.entity;

import com.github.xgf.common.base.BaseEntity;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;



@Entity
@Table(name = "school")
@Data
@SQLDelete(sql = "update school set delete_flag = 1,delete_date = now(), delete_user = ?2   where id = ?1 ")
@Where(clause = "delete_flag = 0")
public class School extends BaseEntity<User> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    public School() {
    }


}