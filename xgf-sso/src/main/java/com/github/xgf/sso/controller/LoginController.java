package com.github.xgf.sso.controller;

import com.github.xgf.common.result.ResultBean;
import com.github.xgf.common.status.ResultStatus;
import com.github.xgf.common.util.JWT;
import com.github.xgf.sso.dto.UserLoginResult;
import com.github.xgf.sso.model.Login;
import com.github.xgf.sso.repository.UserRespository;
import com.github.xgf.sso.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;


@RestController
public class LoginController {
    @Autowired
    private UserRespository userRespository;

    @Autowired
    @Qualifier("consumerTokenServices")
    ConsumerTokenServices consumerTokenServices;

    //处理登录
    @RequestMapping(value = "/login", produces = "application/json; charset=utf-8")
    public ResultBean login(HttpServletRequest request, @RequestParam("username") String username,
                            @RequestParam("password") String password) {
        User user = userRespository.findByUsername(username);
        //封装成对象返回给客户端
        UserLoginResult userLoginResult = new UserLoginResult();
        if (null != user) {
            Login login = new Login();
            login.setPassword(password);
            login.setId(user.getUserID());
            //给用户jwt加密生成token
            String token = JWT.sign(login, 60L * 1000L * 30L);

            userLoginResult.setLoginId(login.getId());
            userLoginResult.setToken(token);
            userLoginResult.setUser(user);
        } else {
            return new ResultBean(ResultStatus.USERNAME_OR_PASSWORD_ERROR);
        }
        return new ResultBean(userLoginResult);
    }


}
