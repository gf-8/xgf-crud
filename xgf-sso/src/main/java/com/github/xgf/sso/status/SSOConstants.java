package com.github.xgf.sso.status;

public class SSOConstants {
    /**
     * 存放jwt token公用
     */
    public static final String AUTHORIZATION = "authorization";

    /**
     * 存放loginId jwt用
     */
    public static final String JWTLOGINId = "loginId";
}
