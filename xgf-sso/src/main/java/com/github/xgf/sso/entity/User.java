package com.github.xgf.sso.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.xgf.common.base.BaseJsonignoreEntity;
import lombok.Data;
import org.hibernate.annotations.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.CascadeType;
import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "mst_user")
@Data
@Where(clause = "delete_flag = 0")
public class User extends BaseJsonignoreEntity<User> implements UserDetails, Serializable {

	/**
	 * 表格主键Id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;
	@Column
	public String userID;
	@Column
	private String username;
	private String password;
	@Column
	private String roleid;
	@Column
	private String userType;

	@Transient
	private Set<GrantedAuthority> authorities = new HashSet<>();

	@Column
	private String userLogo;
	@Column
	private String phone;
	@Column
	private String icard;
	@Column
	private String email;
	@Column
	private Date lastLoginTime;
	@Column(columnDefinition="INT default 0")
	private int loginCount;
	/**
	 * 角色集合
	 */
	@JsonIgnore
	@ManyToMany(targetEntity = Role.class, fetch = FetchType.EAGER)
	@BatchSize(size = 20)
	private Set<Role> roles = new HashSet<>();

	@JsonIgnore
	@Column
	private String token;
	@JsonIgnore
	@Column
	private Date tokenTime;
	/**
	 * 查询用户的时候的创建人信息
	 */
	@Transient
	private User outCreateUser  = null;
	@Transient
	private User outUpdateUser  = null;
	@Transient
	private User outDeleteUser  = null;
	public User(String userid, String token, Date tokenTime) {
		this.userID = userid;
		this.token = token;
		this.tokenTime = tokenTime;
	}

	public User() {
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
