package com.github.xgf.sso.model;

import lombok.Data;

@Data
public class Login {
    private String id;
    private String password;

}
