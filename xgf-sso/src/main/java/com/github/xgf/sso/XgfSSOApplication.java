package com.github.xgf.sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.boot.Banner;

@SpringBootApplication
@EnableDiscoveryClient
public class XgfSSOApplication {

	public static void main(String[] args) {
		SpringApplication springApplication=new SpringApplication();
		springApplication.setBannerMode(Banner.Mode.CONSOLE);//设置输出模式
		SpringApplication.run(XgfSSOApplication.class, args);
		
	}

}
