package com.github.xgf.sso.entity;

import com.github.xgf.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 *
 * @author xgf
 */
@Entity(name = "mst_authority")
@Data
@EqualsAndHashCode(callSuper = false)
public class Authority extends BaseEntity<User> implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String value;
}
