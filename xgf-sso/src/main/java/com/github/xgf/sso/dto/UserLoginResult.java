package com.github.xgf.sso.dto;

import com.github.xgf.sso.entity.User;
import lombok.Data;

@Data
public class UserLoginResult {
    private String loginId;
    private String token;
    private User user;

}
