package com.github.xgf.sso.config;

import com.alibaba.fastjson.JSONObject;
import com.github.xgf.common.exception.UnloginException;
import com.github.xgf.common.result.ResultBean;
import com.github.xgf.common.status.ResultStatus;
import com.github.xgf.common.util.JWT;
import com.github.xgf.sso.model.Login;
import com.github.xgf.sso.status.SSOConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.lang.reflect.Method;

/**
 * 自定义拦截器，判断此次请求是否有权限
 * @Author xgf
 */
//@Component
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {
    public static void main(String[] args) {
        Login login = JWT.unsign("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MTczODQ1NzAzMDQsInBheWxvYWQiOiJ7XCJpZFwiOlwiMVwiLFwicGFzc3dvcmRcIjpcIjFcIn0ifQ.WkYpose4JQrKTz25LL5RjcXZiPqT1WANGr-4rZzXuNY", Login.class);
        System.out.println(login);
    }

    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        response.setCharacterEncoding("utf-8");
        String token = request.getHeader(SSOConstants.AUTHORIZATION);
        //token不存在
        if(null != token) {
            Login login = JWT.unsign(token, Login.class);
            String loginId = request.getHeader(SSOConstants.JWTLOGINId);
            //解密token后的loginId与用户传来的loginId不一致，一般都是token过期
            if(null != loginId && null != login) {
                if(loginId.equals(login.getId())) {
                    return true;
                }
                else{
                    responseMessage(response, response.getWriter(), new ResultBean(ResultStatus.TOKEN_OVERTIME));
                    return false;
                }
            }
            else
            {
                responseMessage(response, response.getWriter(), new ResultBean(ResultStatus.FAIL));
                return false;
            }
        }
        else
        {
            responseMessage(response, response.getWriter(), new ResultBean(ResultStatus.FAIL));
            return false;
        }
    }

    //请求不通过，返回错误信息给客户端
    private void responseMessage(HttpServletResponse response, PrintWriter out, ResultBean resultBean) {
        response.setContentType("application/json; charset=utf-8");
        String json = JSONObject.toJSONString(resultBean);
        out.print(json);
        out.flush();
        out.close();
    }
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
    }
}
