package com.github.xgf.sso.service;

import com.github.xgf.sso.entity.User;
import com.github.xgf.sso.repository.UserRespository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.List;

import static com.github.xgf.common.util.CheckUtil.check;


@Service
@Slf4j
public class UserServiceImpl  {
    private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    @Autowired
    private UserRespository userRepository;

    public void createUser(User user) {
        User existing = userRepository.findByUsername(user.getUsername());
        Assert.isNull(existing, "用户已经存在: " + user.getUsername());

        String hash = encoder.encode(user.getPassword());
        user.setPassword(hash);
        userRepository.save(user);
        log.info("用户被创建: {}", user.getUsername());
    }


}
